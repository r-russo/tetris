import pygame


class Spritesheet:
    def __init__(self, sheet, sprite_width, sprite_height=None):
        if sprite_height is None:
            sprite_height = sprite_width

        self.sheet = sheet
        self.sprite_width = sprite_width
        self.sprite_height = sprite_height

        self.image = pygame.image.load(self.sheet)

    def get_sprite(self, i, j):
        rect = pygame.rect.Rect((i * self.sprite_width, j * self.sprite_height),
                                (self.sprite_width, self.sprite_height))

        if not self.image.get_rect().contains(rect):
            raise Warning(f"Sprite of out bounds for image with size {self.image.get_rect().size}")

        surface = pygame.Surface((self.sprite_width, self.sprite_height))
        surface.blit(self.image, (0, 0), rect)

        return surface.convert()


class Font(Spritesheet):
    CHARS = r""" !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~¨"""

    def get_images(self, text):
        size = self.image.get_rect().size
        cols = size[0] // self.sprite_width
        images = []
        for char in text:
            if char not in self.CHARS:
                num_char = 0
            else:
                num_char = self.CHARS.index(char)

            i = num_char % cols
            j = num_char // cols
            images.append(self.get_sprite(i, j))

        return images
