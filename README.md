## Tetris clone made in Pygame
Made following [OneLoneCoder's
tutorial](https://www.youtube.com/watch?v=8OK8_tHeCIA) but implemented in
Python

## Dependencies
- `python >= 3.7`
- `pygame 1.9.6`

## How to run
Execute `python main.py` from the repo's root directory
