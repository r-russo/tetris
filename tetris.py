from random import choice

import pygame

from spritesheet import Spritesheet, Font

SHAPE_I = 0
SHAPE_O = 1
SHAPE_T = 2
SHAPE_J = 3
SHAPE_L = 4
SHAPE_S = 5
SHAPE_Z = 6
LINE_FULL = 7
SHAPE_LOSE = 8
WALL = 9
EMPTY = 10

WALL_TOP_RIGHT = 11
WALL_BOTTOM_RIGHT = 12
WALL_BOTTOM_LEFT = 13
WALL_TOP_LEFT = 14
WALL_TOP = 15
WALL_RIGHT = 16
WALL_BOTTOM = 17
WALL_LEFT = 18

X_SCORE = 14
Y_SCORE = 1

X_LEVEL = 1
Y_LEVEL = 0

X_NEXT_PIECE = X_SCORE
Y_NEXT_PIECE = Y_SCORE + 12

SHAPE_COLOR = {
    SHAPE_I: (0, 255, 255),
    SHAPE_O: (255, 255, 0),
    SHAPE_T: (255, 0, 255),
    SHAPE_J: (0, 0, 255),
    SHAPE_L: (255, 127, 0),
    SHAPE_S: (0, 255, 0),
    SHAPE_Z: (255, 0, 0),
    LINE_FULL: (127, 127, 127),
    WALL: (255, 255, 255)
}

PX_SIZE = 32


def get_rotation_ix(i, j, r):
    if r > 3:
        r = r % 4
    if r == 0:
        return 4 * j + i
    elif r == 1:
        return 12 + j - 4 * i
    elif r == 2:
        return 15 - 4 * j - i
    elif r == 3:
        return 3 - j + 4 * i
    else:
        return 0


class Tetris:
    def __init__(self, x, y, high_score, field_width=12, field_height=18):
        self.x = x
        self.y = y
        self.field_width = field_width
        self.field_height = field_height

        self.current_piece = None
        self.next_piece = None
        self.lines_to_clear = []
        self.lose_animation_line = self.field_height - 2

        self.level = 1
        self.current_speed = 60
        self.score = 0
        self.high_score = high_score

        # Load assets
        self.bg = pygame.image.load("assets/bg.png").convert()
        self.spritesheet_blocks = Spritesheet("assets/blocks.png", PX_SIZE)
        self.spritesheet_walls = Spritesheet("assets/walls.png", PX_SIZE)
        self.font = Font("assets/nesfont.png", PX_SIZE)

        self.pieces_images = []
        for i in range(9):
            self.pieces_images.append(self.spritesheet_blocks.get_sprite(i, 0))

        self.walls_images = []
        for i in range(8):
            self.walls_images.append(self.spritesheet_walls.get_sprite(i, 0))

        self.field = [EMPTY] * self.field_width * self.field_height
        for i in range(self.field_width):
            for j in range(self.field_height):
                if i == 0 or i == self.field_width - 1 or j == 0 or j == self.field_height - 1:
                    self.field[j * self.field_width + i] = WALL

        self.possible_pieces = [SHAPE_I, SHAPE_O, SHAPE_T, SHAPE_J, SHAPE_L, SHAPE_S, SHAPE_Z]
        self.create_piece()

    def get_wall_ix(self, i, j, max_i=None, max_j=None):
        if max_i is None:
            max_i = self.field_width
        if max_j is None:
            max_j = self.field_height

        if i == 0 and j == 0:
            return WALL_TOP_LEFT - WALL_TOP_RIGHT
        elif i == max_i - 1 and j == 0:
            return WALL_TOP_RIGHT - WALL_TOP_RIGHT
        elif i == 0 and j == max_j - 1:
            return WALL_BOTTOM_LEFT - WALL_TOP_RIGHT
        elif i == max_i - 1 and j == max_j - 1:
            return WALL_BOTTOM_RIGHT - WALL_TOP_RIGHT
        elif i == 0:
            return WALL_LEFT - WALL_TOP_RIGHT
        elif j == 0:
            return WALL_TOP - WALL_TOP_RIGHT
        elif i == max_i - 1:
            return WALL_RIGHT - WALL_TOP_RIGHT
        else:
            return WALL_BOTTOM - WALL_TOP_RIGHT

    def create_piece(self):
        if self.current_piece is None:  # First piece
            piece = choice(self.possible_pieces)
            self.possible_pieces.remove(piece)
            self.current_piece = Tetromino(self.field_width // 2 - 2, 1, piece)
        else:
            self.current_piece = Tetromino(self.field_width // 2 - 2, 1, self.next_piece.shape)

        self.next_piece = Tetromino(X_NEXT_PIECE, Y_NEXT_PIECE, choice(self.possible_pieces))
        self.possible_pieces.remove(self.next_piece.shape)
        if not self.possible_pieces:
            self.possible_pieces = [SHAPE_I, SHAPE_O, SHAPE_T, SHAPE_J, SHAPE_L, SHAPE_S, SHAPE_Z]

        if not self.current_piece_fit(0, 0):
            self.current_piece = None
            return False

        return True

    def check_line(self):
        lines_to_clear = []
        for j in range(4):
            pos_y = j + self.current_piece.y
            if 1 < pos_y < self.field_height - 1:
                line = True
                for i in range(1, self.field_width - 1):
                    if self.field[pos_y * self.field_width + i] == EMPTY:
                        line = False

                if line:
                    lines_to_clear.append(pos_y)
                    for i in range(1, self.field_width - 1):
                        self.field[pos_y * self.field_width + i] = LINE_FULL

        self.lines_to_clear = lines_to_clear

    def update_score(self):
        self.score += 2 ** (len(self.lines_to_clear) - 1) * (100 + 100 * self.level // 5)
        if self.score > self.high_score:
            self.high_score = self.score

        new_level = self.score // 1000 + 1
        if self.level < new_level:
            self.level = new_level
            self.current_speed -= new_level // 5 * 2

    def clear_line(self):
        if self.lines_to_clear:
            self.update_score()
            for line in self.lines_to_clear:
                if line == 1:
                    for i in range(self.field_width):
                        self.field[line * self.field_width + i] = EMPTY
                else:
                    for j in range(line, 1, -1):
                        for i in range(self.field_width):
                            self.field[j * self.field_width + i] = self.field[(j - 1) * self.field_width + i]

            self.lines_to_clear = []

    def current_piece_fit(self, dx, dy, r=None):
        if r is None:
            r = self.current_piece.rotation
        new_x = self.current_piece.x + dx
        new_y = self.current_piece.y + dy
        for i in range(4):
            for j in range(4):
                ix = get_rotation_ix(i, j, r)
                field_pos = (new_y + j) * self.field_width + (new_x + i)
                if 0 <= new_x + i < self.field_width and 0 <= new_y + j < self.field_height:
                    if self.current_piece.tiles[ix] and self.field[field_pos] != EMPTY:
                        return False

        self.current_piece.x += dx
        self.current_piece.y += dy
        return True

    def lock_current_piece_to_field(self):
        for i in range(4):
            for j in range(4):
                if self.current_piece.tiles[get_rotation_ix(i, j, self.current_piece.rotation)]:
                    self.field[(self.current_piece.y + j) * self.field_width +
                               self.current_piece.x + i] = self.current_piece.shape

    def lose_animation(self):
        if self.lose_animation_line > 0:
            for i in range(1, self.field_width - 1):
                pos_field = self.lose_animation_line * self.field_width + i
                if self.field[pos_field] != EMPTY:
                    self.field[pos_field] = SHAPE_LOSE
            self.lose_animation_line -= 1

    def draw(self, screen):
        # Draw background
        bg_width, bg_height = self.bg.get_rect().size
        screen_width, screen_height = screen.get_rect().size
        for x in range(0, screen_width, bg_width):
            for y in range(0, screen_height, bg_height):
                screen.blit(self.bg, (x, y))

        # Draw field
        for i in range(self.field_width):
            for j in range(self.field_height):
                shape = self.field[j * self.field_width + i]
                if shape != EMPTY and shape != WALL:
                    image = self.pieces_images[shape]
                elif shape == WALL:
                    shape = self.get_wall_ix(i, j)
                    image = self.walls_images[shape]
                else:
                    image = pygame.Surface((PX_SIZE, PX_SIZE))
                    image.fill((0, 0, 0))

                screen.blit(image, ((self.x + i) * PX_SIZE, (self.y + j) * PX_SIZE))

        # Draw current piece
        if self.current_piece is not None:
            for i in range(4):
                for j in range(4):
                    if self.current_piece.tiles[get_rotation_ix(i, j, self.current_piece.rotation)]:
                        image = self.pieces_images[self.current_piece.shape]
                        px = self.current_piece.x + i
                        py = self.current_piece.y + j

                        if 0 < px < self.field_width - 1 and 0 < py < self.field_height - 1:
                            screen.blit(image, ((self.x + px) * PX_SIZE, (self.y + py) * PX_SIZE))

        # Draw next piece
        width, height = (6, 7)
        for i in range(width):
            for j in range(height):
                if i == 0 or j == 0 or i == width - 1 or j == height - 1:
                    shape = self.get_wall_ix(i, j, width, height)
                    image = self.walls_images[shape]
                elif 0 < i < width - 1 and 1 < j < height - 1:
                    if self.next_piece.tiles[get_rotation_ix(i - 1, j - 2, self.next_piece.rotation)]:
                        image = self.pieces_images[self.next_piece.shape]
                    else:
                        image = pygame.Surface((PX_SIZE, PX_SIZE))
                        image.fill((0, 0, 0))
                else:
                    image = pygame.Surface((PX_SIZE, PX_SIZE))
                    image.fill((0, 0, 0))

                px = self.next_piece.x + i
                py = self.next_piece.y + j

                screen.blit(image, (px * PX_SIZE, py * PX_SIZE))
        images_next = self.font.get_images("NEXT")
        for i, image in enumerate(images_next):
            px = self.next_piece.x + 1 + i
            py = self.next_piece.y + 1
            screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        # Draw level
        width, height = (self.field_width, 3)
        for i in range(width):
            for j in range(height):
                if i == 0 or j == 0 or i == width - 1 or j == height - 1:
                    shape = self.get_wall_ix(i, j, width, height)
                    image = self.walls_images[shape]
                else:
                    image = pygame.Surface((PX_SIZE, PX_SIZE))
                    image.fill((0, 0, 0))

                px = X_LEVEL + i
                py = Y_LEVEL + j

                screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        images_level = self.font.get_images("LEVEL: ")
        for i, image in enumerate(images_level):
            px = X_LEVEL + 2 + i
            py = Y_LEVEL + 1
            screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        images_nlvl = self.font.get_images(f"{self.level:02d}")
        for i, image in enumerate(images_nlvl):
            px = X_LEVEL + 8 + i
            py = Y_LEVEL + 1
            screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        # Draw score
        width, height = (8, 9)
        for i in range(width):
            for j in range(height):
                if i == 0 or j == 0 or i == width - 1 or j == height - 1:
                    shape = self.get_wall_ix(i, j, width, height)
                    image = self.walls_images[shape]
                else:
                    image = pygame.Surface((PX_SIZE, PX_SIZE))
                    image.fill((0, 0, 0))

                px = X_SCORE + i
                py = Y_SCORE + j

                screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        images_top = self.font.get_images("TOP")
        for i, image in enumerate(images_top):
            px = X_SCORE + 1 + i
            py = Y_SCORE + 2
            screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        images_highscore = self.font.get_images(f"{self.high_score:06d}")
        for i, image in enumerate(images_highscore):
            px = X_SCORE + 1 + i
            py = Y_SCORE + 3
            screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        images_score = self.font.get_images("SCORE")
        for i, image in enumerate(images_score):
            px = X_SCORE + 1 + i
            py = Y_SCORE + 5
            screen.blit(image, (px * PX_SIZE, py * PX_SIZE))

        images_cur_score = self.font.get_images(f"{self.score:06d}")
        for i, image in enumerate(images_cur_score):
            px = X_SCORE + 1 + i
            py = Y_SCORE + 6
            screen.blit(image, (px * PX_SIZE, py * PX_SIZE))


class Tetromino:
    def __init__(self, x, y, shape):
        self.x = x
        self.y = y
        self.shape = shape
        self.color = SHAPE_COLOR[shape]
        self.rotation = 0
        if shape == SHAPE_I:
            self.tiles = [0, 0, 0, 1,
                          0, 0, 0, 1,
                          0, 0, 0, 1,
                          0, 0, 0, 1]
        elif shape == SHAPE_O:
            self.tiles = [0, 0, 0, 0,
                          0, 1, 1, 0,
                          0, 1, 1, 0,
                          0, 0, 0, 0]
        elif shape == SHAPE_T:
            self.tiles = [0, 0, 1, 0,
                          0, 1, 1, 0,
                          0, 0, 1, 0,
                          0, 0, 0, 0]
        elif shape == SHAPE_J:
            self.tiles = [0, 0, 1, 0,
                          0, 0, 1, 0,
                          0, 1, 1, 0,
                          0, 0, 0, 0]
        elif shape == SHAPE_L:
            self.tiles = [0, 1, 0, 0,
                          0, 1, 0, 0,
                          0, 1, 1, 0,
                          0, 0, 0, 0]
        elif shape == SHAPE_S:
            self.tiles = [0, 0, 1, 0,
                          0, 1, 1, 0,
                          0, 1, 0, 0,
                          0, 0, 0, 0]
        elif shape == SHAPE_Z:
            self.tiles = [0, 1, 0, 0,
                          0, 1, 1, 0,
                          0, 0, 1, 0,
                          0, 0, 0, 0]

        else:
            self.tiles = [0] * 16
