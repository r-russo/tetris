from pathlib import Path
import sys

import pygame

from tetris import PX_SIZE, Tetris

DELAY_HOLD = 20
DELAY_CLEAR = 100
DELAY_LOSE = 100


def run():
    pygame.init()
    clock = pygame.time.Clock()

    high_score_file = Path('high_score')
    if high_score_file.exists():
        with open(high_score_file, 'rb') as f:
            high_score = int.from_bytes(f.read(), byteorder='big')
            if high_score >= 999999:
                high_score = 999999
    else:
        high_score = 0

    w, h = (23 * PX_SIZE, 22 * PX_SIZE)
    screen = pygame.display.set_mode((w, h))
    pygame.display.set_caption("Tetris")

    tetris = Tetris(1, 3, high_score)

    pressed = {
        pygame.K_RIGHT: False,
        pygame.K_DOWN: False,
        pygame.K_LEFT: False,
        pygame.K_UP: False
    }

    hold_rotation = False

    current_delay_hold = pygame.time.get_ticks()
    current_delay_clear = pygame.time.get_ticks()
    current_delay_lose = pygame.time.get_ticks()
    ticks_cnt = 0
    keep_playing = True
    quit_game = False
    while True:
        clock.tick(60)
        ticks_cnt += 1
        force_down = ticks_cnt == tetris.current_speed

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game = True

            elif event.type == pygame.KEYDOWN:
                if event.key in pressed.keys():
                    pressed[event.key] = True
                    current_delay_hold = pygame.time.get_ticks()
            elif event.type == pygame.KEYUP:
                if event.key in pressed.keys():
                    pressed[event.key] = False
                    if event.key == pygame.K_UP:
                        hold_rotation = False

        if pygame.time.get_ticks() - current_delay_hold > DELAY_HOLD and keep_playing:
            if pressed[pygame.K_LEFT]:
                tetris.current_piece_fit(-1, 0)
            elif pressed[pygame.K_RIGHT]:
                tetris.current_piece_fit(1, 0)
            elif pressed[pygame.K_DOWN]:
                if not tetris.current_piece_fit(0, 1):
                    force_down = True
            elif pressed[pygame.K_UP] and not hold_rotation:
                hold_rotation = True
                if tetris.current_piece_fit(0, 0, tetris.current_piece.rotation + 1):
                    tetris.current_piece.rotation += 1
            current_delay_hold = pygame.time.get_ticks()

        if pygame.time.get_ticks() - current_delay_clear > DELAY_CLEAR:
            tetris.clear_line()
        if force_down and keep_playing:
            ticks_cnt = 0
            if not tetris.current_piece_fit(0, 1):
                tetris.lock_current_piece_to_field()
                tetris.check_line()
                keep_playing = tetris.create_piece()
                current_delay_clear = pygame.time.get_ticks()

            if not keep_playing:
                current_delay_lose = pygame.time.get_ticks()
        elif not keep_playing:
            if pygame.time.get_ticks() - current_delay_lose > DELAY_LOSE:
                current_delay_lose = pygame.time.get_ticks()
                tetris.lose_animation()

        if quit_game:
            break
        screen.fill((0, 0, 0))
        tetris.draw(screen)
        pygame.display.update()

    with open(high_score_file, 'wb') as f:
        f.write(tetris.high_score.to_bytes(4, byteorder='big', signed=False))
    pygame.quit()
    sys.exit(0)


if __name__ == '__main__':
    run()
